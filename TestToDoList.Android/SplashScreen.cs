﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.Platforms.Android.Views;

namespace TestToDoList.Android
{
    [MvxActivityPresentation]
    [Activity(
        MainLauncher = true,
        Icon = "@drawable/icon",
        Theme = "@style/SplashScreenTheme",
        NoHistory = true,
        ScreenOrientation = ScreenOrientation.Portrait)]
    public class SplashScreen : MvxSplashScreenActivity
    {
        private LinearLayout _background;
        private ImageView _imageLogo;
        private AnimationDrawable _backgroundAnimation;

        public SplashScreen()
            : base(Resource.Layout.SplashScreen)
        {
        }

        protected override void OnResume()
        {
            base.OnResume();
            
            _background = FindViewById<LinearLayout>(Resource.Id.background);
            _backgroundAnimation = (AnimationDrawable)_background.Background;
            _backgroundAnimation.SetEnterFadeDuration(1500);
            _backgroundAnimation.SetExitFadeDuration(1500);
            _backgroundAnimation.Start();
        }
    }
}