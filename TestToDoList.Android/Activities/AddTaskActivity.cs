﻿using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.AppCompat;
using TestToDoList.Core.ViewModels;
using static Android.App.DatePickerDialog;
using Toolbar = Android.Support.V7.Widget.Toolbar;

namespace TestToDoList.Android.Activities
{
    [Activity(Label = "Add task")]
    public class AddTaskActivity : MvxAppCompatActivity<AddTaskViewModel>, IOnDateSetListener
    {
        private Toolbar _toolbar;
        private EditText _txbDeadline, _txbHeading, _txbDescription;
        private TextView _lblAlert;

        private const int DATE_PICKER_DIALOG = 1;
        private int _year = DateTime.Today.Year, _month = DateTime.Today.Month, _day = DateTime.Today.Day;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.AddTaskView);

            _toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);
            _txbDeadline = FindViewById<EditText>(Resource.Id.txbDeadline);
            _txbHeading = FindViewById<EditText>(Resource.Id.txbHeading);
            _txbDescription = FindViewById<EditText>(Resource.Id.txbDescription);
            _lblAlert = FindViewById<TextView>(Resource.Id.lblAlert);

            var set = this.CreateBindingSet<AddTaskActivity, AddTaskViewModel>();

            set.Bind(_txbDeadline).To(vm => vm.Deadline);
            set.Bind(_txbHeading).To(vm => vm.Heading);
            set.Bind(_txbDescription).To(vm => vm.Description);
            set.Bind(_lblAlert).To(vm => vm.AlertMessage);

            set.Apply();

            SetSupportActionBar(_toolbar);

            Spinner spinner = FindViewById<Spinner>(Resource.Id.spinnerStatus);
            spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.status_array, Resource.Layout.support_simple_spinner_dropdown_item);

            adapter.SetDropDownViewResource(Resource.Layout.support_simple_spinner_dropdown_item);
            spinner.Adapter = adapter;

            HideSoftKeyboard();

            _txbDeadline.Touch += (sender, e) =>
            {
                ShowDialog(DATE_PICKER_DIALOG);
            };
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            _toolbar.InflateMenu(Resource.Menu.edit_menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_save:
                    ViewModel.CheckFieldsCommand.Execute(null);
                    if (string.IsNullOrWhiteSpace(_lblAlert.Text))
                        ViewModel.SaveTaskCommand.Execute(null);
                    break;
                case Resource.Id.menu_return:
                    ViewModel.CloseView(null);
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        public void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            _year = year;
            _month = month;
            _day = dayOfMonth;
            _txbDeadline.Text = $"{_year}/{_month}/{_day}";
        }

        [Obsolete]
        protected override Dialog OnCreateDialog(int id)
        {
            switch(id)
            {
                case DATE_PICKER_DIALOG:
                    return new DatePickerDialog(this, this, _year, _month, _day);
                default:
                    return base.OnCreateDialog(id);
            }
        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            ViewModel.Status = e.Position;
        }

        private void HideSoftKeyboard()
        {
            if (CurrentFocus == null)
                return;

            InputMethodManager inputMethodManager = (InputMethodManager)GetSystemService(InputMethodService);
            inputMethodManager.HideSoftInputFromWindow(CurrentFocus.WindowToken, 0);

            CurrentFocus.ClearFocus();
        }
    }

}