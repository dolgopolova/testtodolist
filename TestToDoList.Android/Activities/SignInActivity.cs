﻿using Android;
using Android.App;
using Android.OS;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views;
using System;
using TestToDoList.Core.ViewModels;

namespace TestToDoList.Android
{
    [Activity(Label = "Sign in")]
    public class SignInActivity : MvxActivity<SignInViewModel>
    {
        private Button _btnSignIn, _btnSignUp;
        private EditText _txbLogin, _txbPassword;
        private TextView _lblAlert;
        private ImageView _imgProfile;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
       
            SetContentView(Resource.Layout.SignInView);

            _btnSignIn = FindViewById<Button>(Resource.Id.btnSignIn);
            _btnSignUp = FindViewById<Button>(Resource.Id.btnSignUp);
            _txbLogin = FindViewById<EditText>(Resource.Id.txbLogin);
            _txbPassword = FindViewById<EditText>(Resource.Id.txbPassword);
            _lblAlert = FindViewById<TextView>(Resource.Id.lblAlert);
            _imgProfile = FindViewById<ImageView>(Resource.Id.imgProfile);
            
            var set = this.CreateBindingSet<SignInActivity, SignInViewModel>();

            set.Bind(_txbLogin).To(vm => vm.Login);
            set.Bind(_txbPassword).To(vm => vm.Password);
            set.Bind(_btnSignUp).To(vm => vm.SignUpCommand);
            set.Bind(_lblAlert).To(vm => vm.AlertMessage);

            set.Apply();

            _btnSignIn.Click += (sender, args) => 
            {
                ViewModel.Validate();
                ViewModel.SignInCommand.Execute(null);
            };
            _txbLogin.AfterTextChanged += (sender, args) => { ViewModel.AlertMessage = ""; };
            _txbPassword.AfterTextChanged += (sender, args) =>{ ViewModel.AlertMessage = "";};
        }
    }
}