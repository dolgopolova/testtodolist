﻿using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Support.V4.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using TestToDoList.Core.ViewModels;
using Android.Support.V4.View;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using Android.Views.InputMethods;

namespace TestToDoList.Android.Views
{
    [MvxActivityPresentation]
    [Activity(Theme = "@style/MyTheme")]
    [Obsolete("Message")]
    public class HomeView : MvxAppCompatActivity<HomeViewModel>
    {
        public DrawerLayout DrawerLayout { get; set; }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.HomeView);

            DrawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);

            if (savedInstanceState == null)
            {
                ViewModel.ShowTasksViewModelCommand.Execute(null);
                ViewModel.ShowMenuViewModelCommand.Execute(null);
            }
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Android.Resource.Id.home:
                    DrawerLayout.OpenDrawer(GravityCompat.Start);
                    return true;
            }
            return base.OnOptionsItemSelected(item);
        }

        public override void OnBackPressed()
        {
            if (DrawerLayout != null && DrawerLayout.IsDrawerOpen(GravityCompat.Start))
                DrawerLayout.CloseDrawers();
            else
                base.OnBackPressed();
        }

        public void HideSoftKeyboard()
        {
            if (CurrentFocus == null)
                return;

            InputMethodManager inputMethodManager = (InputMethodManager)GetSystemService(InputMethodService);
            inputMethodManager.HideSoftInputFromWindow(CurrentFocus.WindowToken, 0);

            CurrentFocus.ClearFocus();
        }
    }
}