﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views;
using TestToDoList.Core.ViewModels;
using Android.Graphics;
using System.IO;
using Android.Provider;
using Android.Support.Design.Widget;
using Android.Support.V4.Widget;

namespace TestToDoList.Android
{
    [Activity(Label = "Sign Up")]
    public class SignUpActivity : MvxActivity<SignUpViewModel>
    {
        ImageButton _imgProfile;
        TextView _lblAlert, _lblImagePath;
        EditText _txbEmail, _txbLogin, _txbPassword, _txbConfirmPassword, _txbFirstName, _txbLastName;
        Button _btnConfirm;
        DrawerLayout _drawerLayout;
        public static readonly int PickImageId = 0;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.SignUpView);

            _drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            _imgProfile = FindViewById<ImageButton>(Resource.Id.imgProfile);
            _lblImagePath = FindViewById<TextView>(Resource.Id.lblImagePath);
            _lblAlert = FindViewById<TextView>(Resource.Id.lblAlert);
            _txbEmail = FindViewById<EditText>(Resource.Id.txbEmail);
            _txbLogin = FindViewById<EditText>(Resource.Id.txbLogin);
            _txbPassword = FindViewById<EditText>(Resource.Id.txbPassword);
            _txbConfirmPassword = FindViewById<EditText>(Resource.Id.txbConfirmPassword);
            _txbFirstName = FindViewById<EditText>(Resource.Id.txbFirstName);
            _txbLastName = FindViewById<EditText>(Resource.Id.txbLastName);
            _btnConfirm = FindViewById<Button>(Resource.Id.btnConfirm);

            var set = this.CreateBindingSet<SignUpActivity, SignUpViewModel>();
            set.Bind(_lblImagePath).To(vm => vm.FileName);
            set.Bind(_lblAlert).To(vm => vm.AlertMessage);
            set.Bind(_txbEmail).To(vm => vm.Email);
            set.Bind(_txbLogin).To(vm => vm.Login);
            set.Bind(_txbPassword).To(vm => vm.Password);
            set.Bind(_txbConfirmPassword).To(vm => vm.ConfirmPassword);
            set.Bind(_txbFirstName).To(vm => vm.UserName);
            set.Bind(_txbLastName).To(vm => vm.LastName);
            set.Apply();

            _txbEmail.EditorAction += (sender, args) => { ViewModel.CheckEmail(); };
            _txbLogin.AfterTextChanged += (sender, args) => { ViewModel.CheckLogin(); };
            _btnConfirm.Click += (sender, args) =>
            {
                ViewModel.Validate();
                if(string.IsNullOrWhiteSpace(_lblAlert.Text))
                    ViewModel.ConfirmSignUpCommand.Execute(null);
            };
            _imgProfile.Click += (sender, args) =>
            {
                ViewModel.UploadImageCommand.Execute(null);
            };
        }
    }
}