﻿using System;

using Android.App;
using Android.OS;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.AppCompat;
using TestToDoList.Core.ViewModels;
using static Android.App.DatePickerDialog;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace TestToDoList.Android.Activities
{
    [Activity(Label = "Edit task")]
    public class EditTaskActivity : MvxAppCompatActivity<EditTaskViewModel>, IOnDateSetListener
    {
        private V7Toolbar _toolbar;
        private EditText _txbDeadline, _txbHeading, _txbDescription;
        private TextView _lblAlert;
        private Spinner _spinner;
        private FloatingActionButton _floatingButton;

        private const int DATE_PICKER_DIALOG = 1;
        private int _year = DateTime.Today.Year, _month = DateTime.Today.Month, _day = DateTime.Today.Day;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.EditTaskView);

            _toolbar = FindViewById<V7Toolbar>(Resource.Id.toolbar);
            _txbDeadline = FindViewById<EditText>(Resource.Id.txbDeadline);
            _txbHeading = FindViewById<EditText>(Resource.Id.txbHeading);
            _txbDescription = FindViewById<EditText>(Resource.Id.txbDescription);
            _lblAlert = FindViewById<TextView>(Resource.Id.lblAlert);
            _floatingButton = FindViewById<FloatingActionButton>(Resource.Id.floating_button);

            var set = this.CreateBindingSet<EditTaskActivity, EditTaskViewModel>();

            set.Bind(_txbDeadline).To(vm => vm.Deadline);
            set.Bind(_txbHeading).To(vm => vm.Heading);
            set.Bind(_txbDescription).To(vm => vm.Description);
            set.Bind(_lblAlert).To(vm => vm.AlertMessage);

            set.Apply();

            _spinner = FindViewById<Spinner>(Resource.Id.spinnerStatus);
            _spinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs>(spinner_ItemSelected);
            var adapter = ArrayAdapter.CreateFromResource(
                    this, Resource.Array.status_array, Resource.Layout.support_simple_spinner_dropdown_item);

            adapter.SetDropDownViewResource(Resource.Layout.support_simple_spinner_dropdown_item);
            _spinner.Enabled = false;
            _spinner.Adapter = adapter;
            _spinner.SetSelection(ViewModel.Status);
            
            _txbDeadline.Touch += (sender, e) =>
            {
                ShowDialog(DATE_PICKER_DIALOG);
            };

            _txbHeading.TextChanged += (sender, e) =>
            {
                _txbHeading.SetSelection(_txbHeading.Text.Length);
            };

            _txbDescription.TextChanged += (sender, e) =>
            {
                _txbDescription.SetSelection(_txbDescription.Text.Length);
            };

            _floatingButton.Click += (sender, args) =>
            {
                SetSupportActionBar(_toolbar);
                _floatingButton.Visibility = ViewStates.Invisible;
                SetEnableStatus(true);
            };
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            _toolbar.InflateMenu(Resource.Menu.edit_menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case Resource.Id.menu_save:
                    ViewModel.isAndroidPlatform = true;
                    ViewModel.CheckFieldsCommand.Execute(null);
                    if (string.IsNullOrWhiteSpace(_lblAlert.Text))
                        ViewModel.CompleteEditTaskCommand.Execute(null);
                    break;
                case Resource.Id.menu_return:
                    ViewModel.CloseView(null);
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        [Obsolete]
        protected override Dialog OnCreateDialog(int id)
        {
            switch (id)
            {
                case DATE_PICKER_DIALOG:
                    return new DatePickerDialog(this, this, _year, _month, _day);
                default:
                    return base.OnCreateDialog(id);
            }
        }

        public void OnDateSet(DatePicker view, int year, int month, int dayOfMonth)
        {
            _year = year;
            _month = month;
            _day = dayOfMonth;
            _txbDeadline.Text = $"{_year}/{_month}/{_day}";
        }

        private void spinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            ViewModel.Status = e.Position;
        }

        private void HideSoftKeyboard()
        {
            if (CurrentFocus == null)
                return;

            InputMethodManager inputMethodManager = (InputMethodManager)GetSystemService(InputMethodService);
            inputMethodManager.HideSoftInputFromWindow(CurrentFocus.WindowToken, 0);

            CurrentFocus.ClearFocus();
        }

        private void SetEnableStatus(bool isEnabled)
        {
            _txbDeadline.Enabled = isEnabled;
            _txbHeading.Enabled = isEnabled;
            _txbDescription.Enabled = isEnabled;
            _spinner.Enabled = isEnabled;
        }
    }
}