﻿using Android.Graphics;
using static Android.Graphics.Bitmap;
using static Android.Graphics.PorterDuff;

namespace TestToDoList.Android.Service
{
    public class ImageHelper
    {
        public static Bitmap GetRoundedCornerBitmap(Bitmap bitmap, int pixels)
        {
            var size = (bitmap.Height < bitmap.Width) ? bitmap.Height : bitmap.Width;

            Bitmap output = Bitmap.CreateBitmap(size, size, Config.Argb8888);
            Canvas canvas = new Canvas(output);

            Paint paint = new Paint();
            Rect rect = new Rect(0, 0, size, size);
            RectF rectF = new RectF(rect);
            float roundPx = pixels;

            paint.AntiAlias = true;
            canvas.DrawARGB(0, 0, 0, 0);
            paint.Color = Color.Black;
            canvas.DrawRoundRect(rectF, roundPx, roundPx, paint);

            paint.SetXfermode(new PorterDuffXfermode(Mode.SrcIn));
            canvas.DrawBitmap(bitmap, rect, rect, paint);

            return output;
        }
    }
}