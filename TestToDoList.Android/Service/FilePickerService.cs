﻿using MvvmCross.Plugin.File;
using MvvmCross.Plugin.PictureChooser;
using System;
using System.IO;
using System.Threading.Tasks;
using TestToDoList.Core.Models;
using TestToDoList.Core.Services;

namespace TestToDoList.Android
{
    public class FilePickerService : IFilePickerService
    {
        private readonly IDbService _dbService;
        private readonly IMvxPictureChooserTask _pictureCooserTask;
        private readonly IMvxFileStore _fileStore;

        public FilePickerService(IDbService dbService, IMvxPictureChooserTask pictureChooserTask, IMvxFileStore fileStore)
        {
            _dbService = dbService;
            _pictureCooserTask = pictureChooserTask;
            _fileStore = fileStore;
        }

        public Task<PickedFileModel> UploadImageAsync()
        {
            var source = new TaskCompletionSource<PickedFileModel>();
            _pictureCooserTask.ChoosePictureFromLibrary(400, 95, 
            stream => {
                var memoryStream = new MemoryStream();
                stream.CopyTo(memoryStream);
                byte[] bytes = memoryStream.ToArray();
                source.SetResult(new PickedFileModel
                {
                    Name = GenerateImagePath(bytes),
                    ImageBytes = bytes
                });
            },
            () => { source.SetResult(null); });
            return source.Task;
        }
        
        private string GenerateImagePath(byte[] bytes)
        {
            if (bytes == null)
                return null;

            var randomFileName = "Image" + Guid.NewGuid().ToString("N") + ".jpg";
            _fileStore.EnsureFolderExists("Images");
            var path = _fileStore.PathCombine("Images/", randomFileName);
            _fileStore.WriteFile(path, bytes);

            return path;
        }
    }
}