﻿using System;
using System.Threading.Tasks;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.Graphics.Drawable;
using Android.Views;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using TestToDoList.Android.Views;
using TestToDoList.Core.ViewModels;

namespace TestToDoList.Android.Fragments
{
    [MvxFragmentPresentation(typeof(HomeViewModel), Resource.Id.navigation_frame)]
    [Register("TestToDoList.Android.Fragments.MenuView")]
    public class MenuFragment : MvxFragment<MenuViewModel>, NavigationView.IOnNavigationItemSelectedListener
    {
        private NavigationView _navigationView;
        private IMenuItem _previousMenuItem;
   
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var ignore = base.OnCreateView(inflater, container, savedInstanceState);

            var view = this.BindingInflate(Resource.Layout.MenuView, null);

            _navigationView = view.FindViewById<NavigationView>(Resource.Id.nav_view);
            _navigationView.SetNavigationItemSelectedListener(this);
            _navigationView.Menu.FindItem(Resource.Id.nav_tasks).SetChecked(true);

            var iconTasks = _navigationView.Menu.FindItem(Resource.Id.nav_tasks);
            iconTasks.SetTitle("Tasks");
            iconTasks.SetCheckable(false);
            iconTasks.SetChecked(true);
            _navigationView.SetNavigationItemSelectedListener(this);

            _previousMenuItem = iconTasks;

            var iconNews =_navigationView.Menu.FindItem(Resource.Id.nav_news);
            iconNews.SetTitle("News");
            iconNews.SetCheckable(false);

            var iconUserProfile = _navigationView.Menu.FindItem(Resource.Id.nav_profile);
            iconUserProfile.SetTitle("Profile");
            iconUserProfile.SetCheckable(false);

            return view;
        }

        public bool OnNavigationItemSelected(IMenuItem item)
        {
            if (_previousMenuItem != null)
                _previousMenuItem.SetChecked(false);

            item.SetCheckable(true);
            item.SetChecked(true);

            _previousMenuItem = item;

            Navigate(item.ItemId);

            return true;
        }

        private async Task Navigate(int itemId)
        {
            ((HomeView)Activity).DrawerLayout.CloseDrawers();
            await Task.Delay(TimeSpan.FromMilliseconds(250));

            switch (itemId)
            {
                case Resource.Id.nav_tasks:
                    ViewModel.ShowTasksCommand.Execute(null);
                    break;
                case Resource.Id.nav_news:
                    ViewModel.ShowNewsCommand.Execute(null);
                    break;
                case Resource.Id.nav_profile:
                    ViewModel.ShowUserProfileCommand.Execute(null);
                    break;
            }
        }
    }
}