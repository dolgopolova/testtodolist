﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Commands;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using TestToDoList.Android.Fragments;
using TestToDoList.Core.ViewModels;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;

namespace TestToDoList.Android.Activities.Fragments
{
    [MvxFragmentPresentation(typeof(HomeViewModel), Resource.Id.fragmentContainer, true)]
    [Register("TestToDoList.Android.Activities.Fragments.NewsFragment")]
    public class NewsFragment : BaseFragment<NewsViewModel>
    {
        protected override int FragmentId => Resource.Layout.NewsFragmentView;

        public ICommand RefreshNewsCommand { get; private set; }

        private V7Toolbar _toolbar;
        private MvxSwipeRefreshLayout _refresher;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RefreshNewsCommand = new MvxAsyncCommand(RefreshNewsAsync);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            ParentActivity.SupportActionBar.Title = "News";

            _refresher = view.FindViewById<MvxSwipeRefreshLayout>(Resource.Id.refresher);
            _refresher.RefreshCommand = RefreshNewsCommand;

            _toolbar = view.FindViewById<V7Toolbar>(Resource.Id.toolbar);
            this.AddBindings(_toolbar, "Tasks");
            this.AddBindings(this, "Interaction Interaction");

            var recyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.tasks_recycler_view);
            if (recyclerView != null)
            {
                var layoutManager = new LinearLayoutManager(Activity);
                recyclerView.SetLayoutManager(layoutManager);
            }

            return view;
        }

        private async Task RefreshNewsAsync()
        {
            ViewModel.News.Clear();
            await ViewModel.Initialize();
            _refresher.Refreshing = false;
        }
    }
}