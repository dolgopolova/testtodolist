﻿using System;

namespace TestToDoList.Android.Fragments.Dialogs
{
    public class PasswordDialogEventArgs
    {
        public string Current { get; set; }
        public string NewPass { get; set; }
        public string ConfirmPass { get; set; }
    }
}