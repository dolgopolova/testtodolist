﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V4;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using TestToDoList.Core.ViewModels;

namespace TestToDoList.Android.Fragments.Dialogs
{
    [MvxFragmentPresentation(typeof(HomeViewModel), Resource.Id.fragmentContainer, true)]
    [Register("TestToDoList.Android.Activities.Fragments.PasswordAlertDialog")]
    public class PasswordAlertDialog : MvxDialogFragment
    {
        private EditText _txbCurrentPassword, _txbNewPassword, _txbConfirmPassword;
        private Button _btnChangePassword, _btnCancel;
        public event EventHandler<PasswordDialogEventArgs> DialogClosed;

        public static PasswordAlertDialog NewInstace(Bundle bundle)
        {
            var fragment = new PasswordAlertDialog();
            fragment.Arguments = bundle;
            return fragment;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            this.EnsureBindingContextIsSet(inflater);
            var view = inflater.Inflate(Resource.Layout.PasswordAlertDialog, container, false);
            Dialog.Window.RequestFeature(WindowFeatures.NoTitle);

            _btnChangePassword = view.FindViewById<Button>(Resource.Id.btnChangePassword);
            _btnCancel = view.FindViewById<Button>(Resource.Id.btnCancel);
            _txbCurrentPassword = view.FindViewById<EditText>(Resource.Id.txbCurrentPassword);
            _txbNewPassword = view.FindViewById<EditText>(Resource.Id.txbNewPassword);
            _txbConfirmPassword = view.FindViewById<EditText>(Resource.Id.txbConfirmPassword);

            _btnChangePassword.Click += Confirm;
            _btnChangePassword.Click += delegate 
            {
                Dismiss();
            };

            _btnCancel.Click += delegate
            {
                Dismiss();
            };
            return view;
        }

        private void Confirm(object sender, EventArgs e)
        {
            DialogClosed?.Invoke(this, new PasswordDialogEventArgs
            {
                Current = _txbCurrentPassword.Text,
                NewPass = _txbNewPassword.Text,
                ConfirmPass = _txbConfirmPassword.Text
            });

        }
    }

}