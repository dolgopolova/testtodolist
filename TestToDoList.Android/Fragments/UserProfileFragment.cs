﻿using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Views;
using Android.Widget;
using MvvmCross.Binding.BindingContext;
using FragmentManager = Android.Support.V4.App.FragmentManager;
using FragmentTransaction = Android.Support.V4.App.FragmentTransaction;
using Fragment = Android.Support.V4.App.Fragment;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using TestToDoList.Android.Fragments;
using TestToDoList.Android.Fragments.Dialogs;
using TestToDoList.Core.ViewModels;
using TestToDoList.Android.Service;
using System;

namespace TestToDoList.Android.Activities.Fragments
{
    [MvxFragmentPresentation(typeof(HomeViewModel), Resource.Id.fragmentContainer, true)]
    [Register("TestToDoList.Android.Activities.Fragments.UserProfileFragment")]
    public class UserProfileFragment : BaseFragment<UserProfileViewModel>
    {
        private ImageButton _btnimgProfile, _btnSetImage;
        private TextView _lblAlert, _txtName;
        private EditText _txbEmail, _txbLogin, _txbFirstName, _txbSecondName;
        private Button _btnChangePassword;
        private FloatingActionButton _floating_button;

        private V7Toolbar _toolbar;

        protected override int FragmentId => Resource.Layout.UserProfileFragmentView;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
        }
        
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            ParentActivity.SupportActionBar.Title = "Profile";

            _toolbar = view.FindViewById<V7Toolbar>(Resource.Id.toolbar);
            this.AddBindings(_toolbar, "Profile");
            this.AddBindings(this, "Interaction Interaction");

            _btnimgProfile = view.FindViewById<ImageButton>(Resource.Id.imgProfile);
            _btnSetImage = view.FindViewById<ImageButton>(Resource.Id.btnUnsplash);
            _lblAlert = view.FindViewById<TextView>(Resource.Id.lblAlert);
            _txbFirstName = view.FindViewById<EditText>(Resource.Id.txbFirstName);
            _txbSecondName = view.FindViewById<EditText>(Resource.Id.txbSecondName);
            _txbEmail = view.FindViewById<EditText>(Resource.Id.txbEmail);
            _txbLogin = view.FindViewById<EditText>(Resource.Id.txbLogin);
            _btnChangePassword = view.FindViewById<Button>(Resource.Id.btnChangePassword);
            _floating_button = view.FindViewById<FloatingActionButton>(Resource.Id.floating_button);
            _txtName = view.FindViewById<TextView>(Resource.Id.txtName);

            var set = this.CreateBindingSet<UserProfileFragment, UserProfileViewModel>();
            set.Bind(_txbFirstName).To(vm => vm.FirstName);
            set.Bind(_txbSecondName).To(vm => vm.LastName);
            set.Bind(_txbEmail).To(vm => vm.Email);
            set.Bind(_txbLogin).To(vm => vm.Login);
            set.Bind(_lblAlert).To(vm => vm.AlertMessage);

            set.Apply();

            SetImage();
            
            _txtName.Text = $"{_txbFirstName.Text} {_txbSecondName.Text}";
            _txbEmail.AfterTextChanged += (sender, args) =>
            {
                ViewModel.CheckEmail();
                ViewModel.AlertMessage = " ";
            };
            _txbLogin.AfterTextChanged += (sender, args) =>
            {
                ViewModel.CheckLogin();
                ViewModel.AlertMessage = " ";
            };

            _btnChangePassword.Click += (sender, args) =>
            {
                ShowCustomAlertDialog();
            };

            _btnimgProfile.Click += async (sender, args) =>
            {
                await ViewModel.UploadImageAsync();
                SetImage();
            };

            _floating_button.Click += (sender, args) =>
            {
                HasOptionsMenu = true;
                _btnChangePassword.Visibility = ViewStates.Visible;
                _btnChangePassword.SetBackgroundColor(Color.Rgb(66, 165, 245));
                _floating_button.Visibility = ViewStates.Invisible;
                SetEnableStatus(true);
            };

            _btnSetImage.Click += async (sender, args) =>
            {
                await ViewModel.UploadImageAsync();
                SetImage();
            };

            return view;
        }

        public override void OnCreateOptionsMenu(IMenu menu, MenuInflater inflater)
        {
            inflater.Inflate(Resource.Menu.edit_menu, menu);

            base.OnCreateOptionsMenu(menu, inflater);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch(item.ItemId)
            {
                case Resource.Id.menu_save:
                    if (string.IsNullOrWhiteSpace(_lblAlert.Text))
                    {
                        HasOptionsMenu = false;
                        SetEnableStatus(false);
                        _floating_button.Visibility = ViewStates.Visible;
                        _btnChangePassword.SetBackgroundColor(Color.Rgb(125, 125, 125));
                        _txtName.Text = $"{_txbFirstName.Text} {_txbSecondName.Text}";
                        ViewModel.EditUserProfile();
                    }
                    break;
                case Resource.Id.menu_return:
                    HasOptionsMenu = false;
                    SetEnableStatus(false);
                    _floating_button.Visibility = ViewStates.Visible;
                    _btnChangePassword.SetBackgroundColor(Color.Rgb(125, 125, 125));
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void SetEnableStatus(bool isEnabled)
        {
            _btnimgProfile.Enabled = isEnabled;
            _btnSetImage.Enabled = isEnabled;
            _lblAlert.Enabled = isEnabled;
            _txbFirstName.Enabled = isEnabled;
            _txbSecondName.Enabled = isEnabled;
            _txbEmail.Enabled = isEnabled;
            _txbLogin.Enabled = isEnabled;
            _btnChangePassword.Enabled = isEnabled;
        }

        private void ShowCustomAlertDialog()
        {
            FragmentTransaction fragmentTransaction = FragmentManager.BeginTransaction();
            Fragment fragmentPrev = FragmentManager.FindFragmentByTag("dialog");
            if (fragmentPrev != null)
                fragmentTransaction.Remove(fragmentPrev);
            fragmentTransaction.AddToBackStack(null);
            PasswordAlertDialog alertDialog = PasswordAlertDialog.NewInstace(null);
            alertDialog.DialogClosed += OnPassworChangeDialogClosed;
            alertDialog.Show(fragmentTransaction, "dialog");
        }

        private void SetImage()
        {
            if (ViewModel.ProfileImage != null)
            {
                var bitmap = BitmapFactory.DecodeByteArray(ViewModel.ProfileImage, 0, ViewModel.ProfileImage.Length);
                bitmap = ImageHelper.GetRoundedCornerBitmap(bitmap, 100);
                _btnimgProfile.SetImageBitmap(bitmap);
            }
        }

        private void OnPassworChangeDialogClosed(object sender, PasswordDialogEventArgs args)
        {
            ViewModel.CurrentPassword = args.Current;
            ViewModel.NewPassword = args.NewPass;
            ViewModel.ConfirmPassword = args.ConfirmPass;
            ViewModel.PasswordValidate();
        }
    }
}