﻿using Android.OS;
using Android.Runtime;
using Android.Views;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using TestToDoList.Android.Fragments;
using TestToDoList.Core.ViewModels;
using V7Toolbar = Android.Support.V7.Widget.Toolbar;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Droid.Support.V7.RecyclerView;
using Android.Support.Design.Widget;
using Android.Support.V7.Widget;
using MvvmCross.Commands;
using System.Threading.Tasks;
using Android.App;

namespace TestToDoList.Android.Activities.Fragments
{
    [MvxFragmentPresentation(typeof(HomeViewModel), Resource.Id.fragmentContainer, true)]
    [Register("TestToDoList.Android.Activities.Fragments.TaskFragment")]
    public class TaskFragment : BaseFragment<TaskViewModel>
    {
        private V7Toolbar _toolbar;
        private FloatingActionButton _floatingActionButton;

        public IMvxCommand<TaskViewItem> DeleteTaskCommand { get; private set; }

        protected override int FragmentId => Resource.Layout.TasksFragmentView;


        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            DeleteTaskCommand = new MvxAsyncCommand<TaskViewItem>(DeleteTaskAsync);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);

            ParentActivity.SupportActionBar.Title = "Tasks";
            
            _toolbar = view.FindViewById<V7Toolbar>(Resource.Id.toolbar);
            this.AddBindings(_toolbar, "Tasks");
            this.AddBindings(this, "Interaction Interaction");

            _floatingActionButton = view.FindViewById<FloatingActionButton>(Resource.Id.floating_button);
            _floatingActionButton.Click += (sender, args) => ViewModel.AddTaskCommand.Execute(null);

            var recyclerView = view.FindViewById<MvxRecyclerView>(Resource.Id.tasks_recycler_view);
            if (recyclerView != null)
            {
                var layoutManager = new LinearLayoutManager(Activity);
                recyclerView.SetLayoutManager(layoutManager);
            }
            recyclerView.ItemLongClick = DeleteTaskCommand;

            return view;
        }

        private async Task DeleteTaskAsync(TaskViewItem arg)
        {
            AlertDialog.Builder alert = new AlertDialog.Builder(Context);
            alert.SetTitle("Confirm delete");
            alert.SetMessage($"Are you sure to delete task with title: {arg.Heading}");
            alert.SetPositiveButton("Delete", (senderAlert, args) => {
                ViewModel.DeleteTask(arg.Id);
                ViewModel.Tasks.Remove(arg);
            });

            alert.SetNegativeButton("Cancel", (senderAlert, args) => {
                
            });

            Dialog dialog = alert.Create();
            dialog.Show();
        }
    }
}