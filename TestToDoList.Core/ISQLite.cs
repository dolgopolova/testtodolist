﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestToDoList.Core
{
    public interface ISQLite
    {
        string GetDatabasePath(string filename);
    }
}
