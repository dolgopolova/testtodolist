﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestToDoList.Core.Models;
using TestToDoList.Core.Services;
//using UIKit;

namespace TestToDoList.Core.ViewModels
{
    public class NewsViewModel: BaseViewModel<User>
    {
        private readonly IRssService _rssService;

        public MvxObservableCollection<News> News { get; set; }

        //public NewsViewModel()
        //{
        //}

        public NewsViewModel(IRssService rssService)
        {
            _rssService = rssService;
            Title = "News";
            News = new MvxObservableCollection<News>();
        }

        public override async Task Initialize()
        {
            var newsList = await _rssService.GetFeedsAsync();
            foreach (var item in newsList.ToList())
            {
                News.Add(item);
            }

            await base.Initialize();
        }

        public override void Prepare(User parameter)
        {
            throw new NotImplementedException();
        }
        
    }
}
