﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using TestToDoList.Core.Models;
using TestToDoList.Core.Services;
using static TestToDoList.Core.ViewModels.UserProfileViewModel;

namespace TestToDoList.Core.ViewModels
{
    public class SignUpViewModel : BaseViewModel<User>
    {
        private string _firstName;
        private string _lastName;
        private string _login;
        private string _password;
        private string _confirmPass;
        private string _email;
        private string _fileName;
        private bool _isValid;
        private byte[] _profileImage;

        private readonly IUserService _userService;
        private readonly IFilePickerService _pickerService;

        public event UploadImageEventHandler UploadImageFinally;

        public ICommand UploadImageCommand => new MvxAsyncCommand(UploadImageAsync);
        public ICommand ConfirmSignUpCommand => new MvxCommand(ConfirmSignupAsync);

        public SignUpViewModel(IMvxNavigationService mvxNavigationService, IUserService userService, IFilePickerService pickerService)
    : base(mvxNavigationService)
        {
            _pickerService = pickerService;
            _userService = userService;
        }
        public override async Task Initialize()
        {
            Title = "Create Account";
            await base.Initialize();
        }

        private async Task UploadImageAsync()
        {
            var pickedFileModel = await _pickerService.UploadImageAsync();
            if (pickedFileModel != null)
            {
                _profileImage = pickedFileModel.ImageBytes;
                FileName = pickedFileModel.Name;
                OnUploadImageFinally();
            }
        }

        public delegate void UploadImageEventHandler();

        protected virtual void OnUploadImageFinally()
        {
            UploadImageFinally?.Invoke();
        }

        private void ConfirmSignupAsync()
        {
            if (!_isValid && !string.IsNullOrEmpty(AlertMessage))
                return;

            var user = new User
            {
                Email = _email,
                Login = _login,
                Name = _firstName,
                LastName = _lastName,
                Password = _password,
                Photo = _profileImage
            };
            _userService.SaveUser(user);
            NavigationService.Navigate<SignInViewModel>();
            base.ViewDispose(this);
            ViewDestroy();
        }
        
        private bool IsPasswordValid()
        {
            if (_password.Length >= 6)
                return true;
            return false;
        }

        public override void Prepare(User parameter)
        {
            throw new NotImplementedException();
        }

        #region checking data
        public void CheckLogin()
        {
            AlertMessage = "";
            if (string.IsNullOrWhiteSpace(Login))
                return;

            var user = _userService.GetUserByLogin(_login);

            AlertMessage = user == null ? "" : "User with such login is already exist!";
        }

        public void CheckEmail()
        {
            AlertMessage = "";
            if (string.IsNullOrWhiteSpace(Email))
                return;

            if (!Regex.Match(_email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
            {
                AlertMessage = "E-mail address is invalid!";
                return;
            }

            var user = _userService.GetUserByEmail(_email);
            AlertMessage = user != null ? AlertMessage = "User with such a mail is already registered!" : "";
        }

        public override void Validate()
        {
            AlertMessage = "";
            if (!string.IsNullOrWhiteSpace(_login) && !string.IsNullOrWhiteSpace(_password) && !string.IsNullOrWhiteSpace(_email))
            {
                _isValid = IsPasswordValid();
                if (!_isValid)
                    AlertMessage = "The password length must be at least 6 characters.";
                _isValid = (_password == _confirmPass) ? true : false;
                if (!_isValid)
                    AlertMessage = "Confirm password does not match the new password.";
            }
            else
            {
                AlertMessage = "Not all required fields are filled!";
            }
        }
        #endregion

        #region properties

        public string UserName
        {
            get => _firstName;
            set
            {
                _firstName = value;
                RaisePropertyChanged();
            }
        }
        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                RaisePropertyChanged();
            }
        }
        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                RaisePropertyChanged();
            }
        }
        public string ConfirmPassword
        {
            get => _confirmPass;
            set
            {
                _confirmPass = value;
                RaisePropertyChanged();
            }
        }
        public string Email
        {
            get => _email;
            set
            {
                _email = value;
                RaisePropertyChanged();
            }
        }
        public string LastName
        {
            get => _lastName;
            set
            {
                _lastName = value;
                RaisePropertyChanged();
            }
        }

        public byte[] ProfileImage
        {
            get => _profileImage;
            set
            {
                _profileImage = value;
                RaisePropertyChanged();
            }
        }

        public string FileName
        {
            get => _fileName;
            set
            {
                _fileName = value;
                RaisePropertyChanged();
            }
        }
        #endregion
    }
}

