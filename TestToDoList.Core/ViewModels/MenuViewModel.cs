﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.ViewModels
{
    public class MenuViewModel : BaseViewModel<User>
    {
        private User _user;
        private readonly IMvxNavigationService _navigationService;

        public User User => _user;

        public MenuViewModel(IMvxNavigationService navigationService)
        {
            _navigationService = navigationService;

            ShowTasksCommand = new MvxAsyncCommand(async () => await _navigationService.Navigate<TaskViewModel, User>(User));
            ShowNewsCommand = new MvxAsyncCommand(async () => await _navigationService.Navigate<NewsViewModel>());
            ShowUserProfileCommand = new MvxAsyncCommand(async () => await _navigationService.Navigate<UserProfileViewModel, User>(User));
        }

        // MvvmCross Lifecycle

        // MVVM Properties

        // MVVM Commands
        public IMvxCommand ShowTasksCommand { get; private set; }
        public IMvxCommand ShowNewsCommand { get; private set; }
        public IMvxCommand ShowUserProfileCommand { get; private set; }

        public override void Prepare(User user)
        {
            _user = user;
            base.Prepare();
        }

        // Private methods
    }
}
