﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TestToDoList.Core.Enums;
using TestToDoList.Core.Models;
using TestToDoList.Core.Services;

namespace TestToDoList.Core.ViewModels
{
    public class EditTaskViewModel : MvxViewModel<TaskModel, TaskModel>
    {
        public bool isAndroidPlatform { get; set; } = false;
        private int _taskModelId;
        private int _userId;
        private string _heading;
        private string _deadline;
        private TaskStatusType _status;
        private string _description;
        private bool _isValid;
        private string _alert;

        private readonly IMvxNavigationService _navigationService;
        public ICommand CheckFieldsCommand => new MvxCommand(CheckFields);
        public ICommand CompleteEditTaskCommand => new MvxCommand(CompleteEditTaskAsync);

        private readonly ITaskService _taskService;

        public override void Prepare(TaskModel taskModel)
        {
            InitTask(taskModel);
            base.Prepare();
        }

        public EditTaskViewModel(IMvxNavigationService mvxNavigationService, ITaskService taskService)
        {
            _navigationService = mvxNavigationService;
            _taskService = taskService;
        }

        private void InitTask(TaskModel taskModel)
        {
            _taskModelId = taskModel.Id;
            _heading = taskModel.Heading;
            _deadline = taskModel.Deadline;
            _status = taskModel.Status;
            _description = taskModel.Description;
            _userId = taskModel.UserId;
        }

        private async void CompleteEditTaskAsync()
        {
            if (isAndroidPlatform)
                _taskModelId += 1;

            var task = new TaskModel
            {
                Id = _taskModelId,
                Heading = _heading,
                Deadline = _deadline,
                Status = _status,
                Description = _description,
                UserId = _userId
            };
            _taskService.UpdateTask(task);

            await CloseView(task);
        }

        public async Task CloseView(TaskModel task)
        {
            await _navigationService.Close(this, task);
        }

        public void OnResume()
        {
            RaiseAllPropertiesChanged();
        }

        private void CheckFields()
        {
            AlertMessage = " ";
            _isValid = (string.IsNullOrWhiteSpace(Heading) || string.IsNullOrWhiteSpace(Deadline)
                || string.IsNullOrWhiteSpace(Description)) ? true : false;

            if (_isValid)
            {
                AlertMessage = "There is empty fields";
                return;
            }
        }

        #region properties

        public string Heading
        {
            get => _heading;
            set
            {
                _heading = value;
                RaisePropertyChanged(); 
            }
        }

        public string Deadline
        {
            get => _deadline;
            set
            {
                _deadline = value;
                RaisePropertyChanged();
            }
        }

        public int Status
        {
            get => (int)_status;
            set
            {
                _status = (TaskStatusType)value;
                RaisePropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        public string AlertMessage
        {
            get => _alert;
            set
            {
                _alert = value;
                RaisePropertyChanged();
            }
        }
        #endregion
    }
}
