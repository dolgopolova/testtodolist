﻿using System.Threading.Tasks;
using MvvmCross.Commands;
using TestToDoList.Core.Models;
using MvvmCross.Navigation;
using TestToDoList.Core.Services;
using System.Linq;

namespace TestToDoList.Core.ViewModels
{
    public class SignInViewModel : BaseViewModel<User>
    {
        private string _login;
        private string _password;
        private bool _isValid;
        private User _user;

        private readonly IUserService _userService;

        public System.Windows.Input.ICommand SignInCommand => new MvxAsyncCommand(LoginAsync);
        public System.Windows.Input.ICommand SignUpCommand => new MvxAsyncCommand(SignUpAsync);

        public SignInViewModel(IMvxNavigationService navigationService, IUserService userService) 
            : base(navigationService)
        {
            _userService = userService;
            Title = "Sign in";
        }
        

        public string Login
        {
            get => _login;
            set
            {
                _login = value;
                RaisePropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                _password = value;
                RaisePropertyChanged();
            }
        }

        private async Task LoginAsync()
        {
            if (_isValid)
            {
                await NavigationService.Navigate<HomeViewModel, User>(_user);
                ViewDispose(this);
            }
        }

        public void OnResume()
        {
            RaiseAllPropertiesChanged();
        }

        private async Task SignUpAsync()
        {
            await NavigationService.Navigate<SignUpViewModel>();
        }

        public override void Validate()
        {
            if (!string.IsNullOrWhiteSpace(_login) && !string.IsNullOrWhiteSpace(_password))
            {
                User user = _userService.GetUserByLoginOrEmail(_login);
                if (user == null)
                {
                    AlertMessage = "User with such login or email does not exist.";
                    return;
                }
                _isValid = user.Password == _password;
                _user = user;
                if (!_isValid)
                {
                    _user = null;
                    AlertMessage = "Invalid login or password!";
                    Password = "";
                }
            }
        }

        public override void Prepare(User parameter)
        {
            throw new System.NotImplementedException();
        }
    }
}
