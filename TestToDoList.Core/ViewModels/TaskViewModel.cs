﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using TestToDoList.Core.Enums;
using TestToDoList.Core.Models;
using TestToDoList.Core.Services;

namespace TestToDoList.Core.ViewModels
{
    public class TaskViewModel : BaseViewModel<User>
    {
        private User _user;

        public User UserModel => _user;
        
        public MvxObservableCollection<TaskViewItem> Tasks{ get; set; }
        private readonly ITaskService _taskService;

        public IMvxCommand AddTaskCommand => new MvxAsyncCommand(AddTaskAsync);
        
        public IMvxCommand<TaskViewItem> TaskSelectedCommand { get; private set; }

        public TaskViewModel(IMvxNavigationService navigationService, ITaskService taskService)
            : base(navigationService)
        {
            Title = "Tasks";
            _taskService = taskService;
            Tasks = new MvxObservableCollection<TaskViewItem>();
            TaskSelectedCommand = new MvxAsyncCommand<TaskViewItem>(TaskSelected);
            
        }

        public TaskViewModel(User user, IMvxNavigationService navigationService, ITaskService taskService)
            : base(navigationService)
        {
            Title = "Tasks";
            _taskService = taskService;
            _user = user;
            Tasks = new MvxObservableCollection<TaskViewItem>();
        }
        
        public override async Task Initialize()
        {
            var taskList = _taskService.SelectAllUserTasks(UserModel);
            foreach (var item in taskList)
            {
                Tasks.Add(new TaskViewItem
                {
                    Id = item.Id,
                    UserId = item.UserId,
                    Heading = item.Heading,
                    Deadline = item.Deadline,
                    Status = item.Status.ToString(),
                    Description = item.Description
                });
            }
            await base.Initialize();
        }

        public void OnResume()
        {
            RaiseAllPropertiesChanged();
        }

        public void DeleteTask(int idTask)
        {
            _taskService.DeleteTask(idTask);
        }

        private async Task TaskSelected(TaskViewItem selectedTask)
        {
            TaskStatusType status = 0;
            switch(selectedTask.Status)
            {
                case "NotStarted":
                    status = TaskStatusType.NotStarted;
                    break;
                case "InProgress":
                    status = TaskStatusType.InProgress;
                    break;
                case "Done":
                    status = TaskStatusType.Done;
                    break;
            }
            TaskModel task = new TaskModel
            {
                UserId = selectedTask.UserId,
                Id = selectedTask.Id - 1,
                Heading = selectedTask.Heading,
                Description = selectedTask.Description,
                Deadline = selectedTask.Deadline,
                Status = status
            };
            EditTaskAsync(task);
        }

        private async Task AddTaskAsync()
        {
            var task = await NavigationService.Navigate<AddTaskViewModel, User, TaskModel>(UserModel);
            if (task != null)
            {
                Tasks.Add(new TaskViewItem
                {
                    Id = task.Id,
                    UserId = task.UserId,
                    Heading = task.Heading,
                    Deadline = task.Deadline,
                    Status = task.Status.ToString(),
                    Description = task.Description
                });
            }
        }

        public async void EditTaskAsync(TaskModel task)
        {
            var newTask = await NavigationService.Navigate<EditTaskViewModel, TaskModel, TaskModel>(task);

            var taskItem = Tasks.SingleOrDefault(t => t?.Id == newTask?.Id);

            if (taskItem != null && newTask != null)
            {
                taskItem.Id = newTask.Id;
                taskItem.UserId = newTask.UserId;
                taskItem.Heading = newTask.Heading;
                taskItem.Deadline = newTask.Deadline;
                taskItem.Status = newTask.Status.ToString();
                taskItem.Description = newTask.Description;
            }
        }

        public override void Prepare(User user)
        {
            _user = user;
            base.Prepare();
        }
    }

    public class TaskViewItem: INotifyPropertyChanged
    {
        private string _heading;
        private string _deadline;
        private string _status;
        private string _description;

        public int Id { get; set; }

        public int UserId { get; set; }

        public string Heading
        {
            get { return _heading; }
            set
            {
                _heading = value;
                PropertyChanged?.Invoke(value, new PropertyChangedEventArgs("Heading"));
            }
        }

        public string Deadline
        {
            get { return _deadline; }
            set
            {
                _deadline = value;
                PropertyChanged?.Invoke(value, new PropertyChangedEventArgs("Deadline"));
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                PropertyChanged?.Invoke(value, new PropertyChangedEventArgs("Status"));
            }
        }

        public string Description
        {
            get { return _description; }
            set
            {
                _description = value;
                PropertyChanged?.Invoke(value, new PropertyChangedEventArgs("Description"));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
