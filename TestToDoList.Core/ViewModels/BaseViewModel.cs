﻿using System;
using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System.Windows.Input;
using MvvmCross.Navigation;
using System.Threading.Tasks;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.ViewModels
{
    public abstract class BaseViewModel<TParametr>: MvxViewModel<TParametr>
    {
        protected readonly IMvxNavigationService NavigationService;
        private string _alert;

        public BaseViewModel()
        {

        }

        public BaseViewModel(IMvxNavigationService navigationService)
        {
            NavigationService = navigationService;
        }
        
        public string Title { get; protected set; }

        public virtual void ViewDispose(IMvxViewModel instance)
        {
            NavigationService.Close(instance);
        }

        public void OnResume()
        {
            RaiseAllPropertiesChanged();
        }

        public string AlertMessage
        {
            get => _alert;
            set
            {
                _alert = value;
                RaisePropertyChanged();
            }
        }
        public override void Prepare(TParametr parametr)
        {
            base.Prepare();
        }

        public virtual void Validate() { }
    }
}
