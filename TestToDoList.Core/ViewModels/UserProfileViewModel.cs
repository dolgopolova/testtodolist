﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using TestToDoList.Core.Models;
using TestToDoList.Core.Services;

namespace TestToDoList.Core.ViewModels
{
    public class UserProfileViewModel : BaseViewModel<User>
    {
        private User _user;
        private bool _isValid;

        private string _currentPassword;
        private string _newPassword;
        private string _confirmPassword;

        private readonly IFilePickerService _pickerService;
        private readonly IUserService _userService;

        public event UploadImageEventHandler UploadImageFinally;

        public UserProfileViewModel(IMvxNavigationService navigationService, IUserService userService, IFilePickerService pickerService)
            : base(navigationService)
        {
            _pickerService = pickerService;
            _userService = userService;
            Title = "Profile";
        }

        public UserProfileViewModel(User user, IMvxNavigationService navigationService, IUserService userService, IFilePickerService pickerService)
            : base(navigationService)
        {
            _pickerService = pickerService;
            _user = user;
            _userService = userService;
            Title = "Profile";
        }

        public void EditUserProfile()
        {
            if (!_isValid && !string.IsNullOrEmpty(AlertMessage))
                return;
           _userService.SaveUser(_user);
        }

        public async Task UploadImageAsync()
        {
            var pickedFileModel = await _pickerService.UploadImageAsync();
            if (pickedFileModel != null)
            {
                _user.Photo = pickedFileModel.ImageBytes;
                _userService.SaveUser(_user);
                OnUploadImageFinally();
            }
        }

        public override void Prepare(User user)
        {
            _user = user;
            base.Prepare();
        }

        public delegate void UploadImageEventHandler();

        protected virtual void OnUploadImageFinally()
        {
            UploadImageFinally?.Invoke();
        }

        #region datachecking

        public void CheckEmail()
        {
            AlertMessage = "";
            if (string.IsNullOrWhiteSpace(Email))
                return;

            if (!Regex.Match(_user.Email, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
            {
                AlertMessage = "E-mail address is invalid!";
                return;
            }
        }

        public void CheckLogin()
        {
            AlertMessage = "";
            if (string.IsNullOrWhiteSpace(Login))
                return;

            var user = _userService.GetUserByLogin(_user.Login);

            AlertMessage = user == null ? "" : "User with such login is already exist!";
        }

        public void PasswordValidate()
        {
            AlertMessage = "";
            _isValid = (_user.Password == _currentPassword) ? true : false;
            if (!_isValid)
            {
                AlertMessage = "Wrong current password.";
                return;
            }

            _isValid = (_newPassword.Length >= 6) ? true : false;
            if (!_isValid)
            {
                AlertMessage = "The password is invalid. The length must be at least 6 characters.";
                return;
            }

            _isValid = (_newPassword == _confirmPassword) ? true : false;
            if (!_isValid)
            {
                AlertMessage = "Confirm password does not match the new password.";
                return;
            }

            _isValid = (_user.Password != _newPassword) ? true : false;
            if (!_isValid)
            {
                AlertMessage = "The new password must not be the same as the old one.";
                return;
            }

            _user.Password = _newPassword;
            _userService.SaveUser(_user);
        }

        #endregion

        #region properties

        public byte[] ProfileImage
        {
            get => _user.Photo;
            set
            {
                _user.Photo = value;
                RaisePropertyChanged();
            }
        }

        public string FirstName
        {
            get => _user.Name;
            set
            {
                _user.Name = value;
                RaisePropertyChanged();
            }
        }

        public string LastName
        {
            get => _user.LastName;
            set
            {
                _user.LastName = value;
                RaisePropertyChanged();
            }
        }

        public string Email
        {
            get => _user.Email;
            set
            {
                _user.Email = value;
                RaisePropertyChanged();
            }
        }
        public string Login
        {
            get => _user.Login;
            set
            {
                _user.Login = value;
                RaisePropertyChanged();
            }
        }
        
        public string CurrentPassword
        {
            get => _currentPassword;
            set
            {
                _currentPassword = value;
                RaisePropertyChanged();
            }
        }

        public string NewPassword
        {
            get => _newPassword;
            set
            {
                _newPassword = value;
                RaisePropertyChanged();
            }
        }

        public string ConfirmPassword
        {
            get => _confirmPassword;
            set
            {
                _confirmPassword = value;
                RaisePropertyChanged();
            }
        }

        public User User { get; set; }

        #endregion
    }
}
