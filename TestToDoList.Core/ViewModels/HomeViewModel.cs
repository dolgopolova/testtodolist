﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.ViewModels
{
    public class HomeViewModel : BaseViewModel<User>
    {
        private User _user;

        public User User => _user;

        public override void Prepare(User user)
        {
            _user = user;
            base.Prepare();
        }

        public HomeViewModel(IMvxNavigationService navigationService): base(navigationService)
        {
            ShowTasksViewModelCommand = new MvxAsyncCommand(async () => await NavigationService.Navigate<TaskViewModel, User>(User));
            ShowNewsViewModelCommand = new MvxAsyncCommand(async () => await NavigationService.Navigate<NewsViewModel>());
            ShowUserProfileViewModelCommand = new MvxAsyncCommand(async () => await NavigationService.Navigate<UserProfileViewModel, User>(User));
            ShowMenuViewModelCommand = new MvxAsyncCommand(async () => await NavigationService.Navigate<MenuViewModel, User>(User));
        }

        public IMvxAsyncCommand ShowTasksViewModelCommand { get; private set; }
        public IMvxAsyncCommand ShowNewsViewModelCommand { get; private set; }
        public IMvxAsyncCommand ShowUserProfileViewModelCommand { get; private set; }
        public IMvxAsyncCommand ShowMenuViewModelCommand { get; private set; }

        public ICommand SettingsCommand => new MvxAsyncCommand(OpenSettingsAsync);

        private async System.Threading.Tasks.Task OpenSettingsAsync()
        {
            await NavigationService.Navigate<SignInViewModel>();
            base.ViewDispose(this);
        }

        public void OnResume()
        {
            RaiseAllPropertiesChanged();
        }
    }
}
