﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using TestToDoList.Core.Enums;
using TestToDoList.Core.Models;
using TestToDoList.Core.Services;

namespace TestToDoList.Core.ViewModels
{
    public class AddTaskViewModel : MvxViewModel<User, TaskModel>
    {
        private string _heading;
        private string _deadline;
        private TaskStatusType _status;
        private string _description;
        private string _alert;
        private int _userId;
        private bool _isValid;

        private readonly ITaskService _taskService;
        private readonly IMvxNavigationService _navigationService;

        public ICommand SaveTaskCommand => new MvxCommand(SaveTaskAsync);
        public ICommand CheckFieldsCommand => new MvxCommand(CheckFields);
        
        public AddTaskViewModel(IMvxNavigationService navigationService, ITaskService taskService)
        {
            _navigationService = navigationService;
            _taskService = taskService;
            _status = TaskStatusType.NotStarted;
        }

        private async void SaveTaskAsync()
        {
            var task = new TaskModel
            {
                Heading = _heading,
                Deadline = _deadline,
                Status = _status,
                Description = _description,
                UserId = _userId
            };
            _taskService.SaveTask(task);
            await CloseView(task);
        }

        public override void Prepare(User user)
        {
            _userId = user.Id;
            base.Prepare();
        }

        public async Task CloseView(TaskModel task)
        {
            await _navigationService.Close(this, task);
        }

        public void OnResume()
        {
            RaiseAllPropertiesChanged();
        }

        private void CheckFields()
        {
            AlertMessage = " ";
            _isValid = (string.IsNullOrWhiteSpace(Heading) || string.IsNullOrWhiteSpace(Deadline)
                || string.IsNullOrWhiteSpace(Description)) ? true : false;

            if (_isValid)
            {
                AlertMessage = "There is empty fields.";
                return;
            }

            if(Status == 2) //Status cannot be done in AddViewModel
            {
                AlertMessage = "You cannot add completed task.";
                return;
            }
        }

        #region properties

        public string Heading
        {
            get => _heading;
            set
            {
                _heading = value;
                RaisePropertyChanged();
            }
        }

        public string Deadline
        {
            get => _deadline;
            set
            {
                _deadline = value;
                RaisePropertyChanged();
            }
        }

        public int Status
        {
            get => (int)_status;
            set
            {
                _status = (TaskStatusType)value;
                RaisePropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                RaisePropertyChanged();
            }
        }

        public string AlertMessage
        {
            get => _alert;
            set
            {
                _alert = value;
                RaisePropertyChanged();
            }
        }
        #endregion
    }
}
