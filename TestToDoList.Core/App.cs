﻿using MvvmCross.IoC;
using MvvmCross.ViewModels;
using TestToDoList.Core.ViewModels;

namespace TestToDoList.Core
{
    public class App : MvxApplication
    {
        public const string DATABASE_NAME = "testtodolistdb.db";

        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<SignInViewModel>();
        }
    }
}
