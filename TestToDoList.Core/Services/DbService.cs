﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MvvmCross;
using SQLite;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.Services
{
    public class DbService: IDbService
    {
        private readonly SQLiteConnection _database;

        public DbService()
        {
            string databasePath = Mvx.IoCProvider.Resolve<ISQLite>().GetDatabasePath(App.DATABASE_NAME);
            _database = new SQLiteConnection(databasePath);
            InitDatabase();
        }

        private void InitDatabase()
        {
            CreateTable<User>();
            CreateTable<TaskModel>();
        }


        #region CRUD operations

        public void CreateTable<T>()
        {
            _database.CreateTable<T>();
        }

        public IEnumerable<T> GetAll<T>() where T : new()
        {
            return (from i in _database.Table<T>() select i).ToList();
        }

        public T Get<T>(int id) where T : new()
        {
            return _database.Get<T>(id);
        }

        public int Delete<T>(int id) where T : Entity
        {
            return _database.Delete<T>(id);
        }

        public int Save<T>(T obj) where T : Entity
        {
            return obj.Id != 0 ? _database.Update(obj) : _database.Insert(obj);
        }

        public IEnumerable<T> Query<T>(string query, params object[] args) where T : new()
        {
            var result = _database.Query<T>(query, args);
            return result;
        }

        #endregion
    }
}
