﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.Services
{
    public interface IRssService
    {
        Task<IEnumerable<News>> GetFeedsAsync();
    }
}
