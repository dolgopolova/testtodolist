﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.Services
{
    public class RssService : IRssService
    {
        private const string API_BASE_URL = "https://api.rss2json.com/v1/api.json?rss_url=";

        public RssService()
        {
        }

        private async Task<string> GetRssJsonAsync()
        {
            var requestUri = new Uri($"{API_BASE_URL}http://digg.com/rss/index.xml");
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync(requestUri);
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();
                return responseBody;
            }
        }

        public async Task<IEnumerable<News>> GetFeedsAsync()
        {
            var json = await GetRssJsonAsync();
            var jObject = (JObject)JsonConvert.DeserializeObject<object>(json);

            var items = jObject["items"]
                .Select(j => new News
                {
                    Title = j["title"].ToString(),
                    Description = j["description"].ToString(),
                    Content = j["content"].ToString(),
                    PublishedDate = j["pubDate"].ToString(),
                    Link = j["link"].ToString()
                })
                .ToList();
            return items;
        }
    }
}
