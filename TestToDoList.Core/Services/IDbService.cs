﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestToDoList.Core.Services
{
    public interface IDbService
    {
        void CreateTable<T>();

        IEnumerable<T> GetAll<T>() where T : new();

        T Get<T>(int id) where T : new();

        int Delete<T>(int id) where T : Models.Entity;
        int Save<T>(T obj) where T : Models.Entity;

        IEnumerable<T> Query<T>(string query, params object[] args) where T : new();
    }
}
