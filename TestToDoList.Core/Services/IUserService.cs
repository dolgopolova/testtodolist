﻿using System;
using System.Collections.Generic;
using System.Text;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.Services
{
    public interface IUserService
    {

        User GetUserByLogin(string login);

        User GetUserByEmail(string email);

        User GetUserByLoginOrEmail(string value);

        void SaveUser(User user);

    }
}
