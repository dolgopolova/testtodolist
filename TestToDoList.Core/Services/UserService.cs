﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.Services
{
    public class UserService : IUserService
    {
        private IDbService _dbService;

        public UserService(IDbService dbService)
        {
            _dbService = dbService;
        }

        public User GetUserByEmail(string email)
        {
            return _dbService.Query<User>("SELECT * FROM Users WHERE Email = ?", email).SingleOrDefault();
        }

        public User GetUserByLogin(string login)
        {
            return _dbService.Query<User>("SELECT * FROM Users WHERE Login = ?", login).SingleOrDefault();
        }

        public User GetUserByLoginOrEmail(string value)
        {
            return _dbService.Query<User>("SELECT * FROM Users WHERE Login = ? OR Email = ? ", value, value).SingleOrDefault();
        }

        public void SaveUser(User user)
        {
            _dbService.Save(user);
        }
    }
}
