﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.Services
{
    public class TaskService : ITaskService
    {
        private IDbService _dbService;

        public TaskService(IDbService dbService)
        {
            _dbService = dbService;
        }

        public void DeleteTask(int idTask)
        {
            _dbService.Delete<TaskModel>(idTask);
        }

        public IEnumerable<TaskModel> SelectAllUserTasks(User user)
        {
            return _dbService.Query<TaskModel>("SELECT * FROM Tasks WHERE UserId = ?", user.Id);
        }

        public void UpdateTask(TaskModel task)
        {
            _dbService.Query<TaskModel>("UPDATE Tasks SET Heading = ?, Deadline = ?, Description = ?, Status = ? WHERE UserId = ? AND Id = ? ",
                task.Heading, task.Deadline, task.Description, task.Status, task.UserId, task.Id);
        }

        public void SaveTask(TaskModel task)
        {
            _dbService.Save(task);
        }
    }
}
