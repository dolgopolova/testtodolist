﻿using System;
using System.Collections.Generic;
using System.Text;
using TestToDoList.Core.Models;

namespace TestToDoList.Core.Services
{
    public interface ITaskService
    {
        IEnumerable<TaskModel> SelectAllUserTasks(User user);

        void DeleteTask(int idTask);

        void UpdateTask(TaskModel Task);

        void SaveTask(TaskModel task);
    }
}
