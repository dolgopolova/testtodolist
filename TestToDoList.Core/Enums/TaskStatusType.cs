﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TestToDoList.Core.Enums
{
    public enum TaskStatusType
    {
        [Description("Not started")]
        NotStarted = 0,
        [Description("In progress")]
        InProgress = 1,
        [Description("Done")]
        Done = 2
    }
}
