﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace TestToDoList.Core.Models
{
    public class Entity
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
    }
}
