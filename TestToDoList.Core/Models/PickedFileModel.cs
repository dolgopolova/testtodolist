﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TestToDoList.Core.Models
{
    public class PickedFileModel
    {
        public byte[] ImageBytes { get; set; }
        public string Name { get; set; }
    }
}
