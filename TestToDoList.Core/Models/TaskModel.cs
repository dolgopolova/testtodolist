﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using TestToDoList.Core.Enums;

namespace TestToDoList.Core.Models
{
    [Table("Tasks")]
    public class TaskModel : Entity
    {
        public string Heading { get; set; }

        public string Deadline { get; set; }

        public string Description { get; set; }

        public TaskStatusType Status { get; set; }

        public int UserId { get; set; }
    }
}
