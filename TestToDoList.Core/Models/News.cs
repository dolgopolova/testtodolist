﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestToDoList.Core.Models
{
    public class News : Entity
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
        public string PublishedDate { get; set; }
        public string Link { get; set; }
    }
}
