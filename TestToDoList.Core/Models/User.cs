﻿using System;
using System.Collections.Generic;
using System.Text;
using SQLite;

namespace TestToDoList.Core.Models
{
    [Table("Users")]
    public class User : Entity
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public byte[] Photo { get; set; }
    }
}
