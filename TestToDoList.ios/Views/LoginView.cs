﻿
using System;
using System.Drawing;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using TestToDoList.Core.ViewModels;
using UIKit;

namespace TestToDoList.ios.Views
{
    public partial class LoginView : MvxViewController<SignInViewModel>
    {
        public LoginView() : base(nameof(LoginView), null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            imgProfile.Image = new UIImage("noun_User.png");

            Title = ViewModel.Title;
            txbUserName.ResignFirstResponder();
            txbPassword.ResignFirstResponder();
            var set = this.CreateBindingSet<LoginView, SignInViewModel>();

            set.Bind(txbUserName).To(vm => vm.Login);
            set.Bind(txbPassword).To(vm => vm.Password);
            set.Bind(btnSignIn).To(vm => vm.SignInCommand);
            set.Bind(btnSignUp).To(vm => vm.SignUpCommand);
            set.Bind(lblAlert).To(vm => vm.AlertMessage);

            set.Apply();

            var viewTap = new UITapGestureRecognizer(() =>
            {
                View.EndEditing(true);
            });
            View.AddGestureRecognizer(viewTap);
            btnSignIn.TouchDown += (sender, args) => { ViewModel.Validate(); };
            txbPassword.EditingDidEndOnExit += (sender, args) => { txbPassword.ResignFirstResponder(); };
            txbUserName.EditingDidBegin += (sender, args) => { ViewModel.AlertMessage = ""; };
            txbPassword.EditingDidBegin += (sender, args) => { ViewModel.AlertMessage = ""; };
        }

        public override void ViewWillAppear(bool animated)
        {
            ViewModel.OnResume();
            base.ViewWillAppear(animated);
        }
        #endregion
    }
}