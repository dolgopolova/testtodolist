﻿
using System;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Input;
using Foundation;
using MvvmCross;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.Platforms.Ios.Views;
using MvvmCross.ViewModels;
using TestToDoList.Core.Models;
using TestToDoList.Core.Services;
using TestToDoList.Core.ViewModels;
using TestToDoList.ios.Views.Tabs;
using UIKit;

namespace TestToDoList.ios.Views
{
    public partial class HomeView : MvxTabBarViewController<HomeViewModel>
    {
        private readonly bool _viewCreated;
        public HomeView()
        {
            ViewControllerSelected += (sender, args) =>
            {
                Title = SelectedViewController.Title;
            };

            _viewCreated = true;
            ViewDidLoad();
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public override void ViewDidLoad()
        {
            if (!_viewCreated)
            {
                return;
            }

            base.ViewDidLoad();

            var viewModel = ViewModel as HomeViewModel;
            Title = "Task";

            var btn = new UIButton();
            btn.TranslatesAutoresizingMaskIntoConstraints = false;
            btn.WidthAnchor.ConstraintEqualTo(32.0f).Active = true;
            btn.HeightAnchor.ConstraintEqualTo(32.0f).Active = true;
            btn.SetImage(new UIImage("icons8-menu-vertical-50.png"), UIControlState.Normal);
            btn.TouchUpInside += (sender, e) => 
            {
                var alert = UIAlertController.Create(null, null, UIAlertControllerStyle.ActionSheet);

                alert.AddAction(UIAlertAction.Create("Settings", UIAlertActionStyle.Default, null));
                alert.AddAction(UIAlertAction.Create("About", UIAlertActionStyle.Default, null));
                alert.AddAction(UIAlertAction.Create("Log out", UIAlertActionStyle.Destructive, action => viewModel?.SettingsCommand.Execute(null)));
                alert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, null));

                PresentViewController(alert, animated: true, completionHandler: null);
            };
            NavigationItem.RightBarButtonItem = new UIBarButtonItem(btn);

            #pragma warning disable CS0618 // Type or member is obsolete
            var navigationService = Mvx.Resolve<IMvxNavigationService>();
            var userService = Mvx.Resolve<IUserService>();
            var taskService = Mvx.Resolve<ITaskService>();
            var rssService = Mvx.Resolve<IRssService>();
            var filePickerService = Mvx.Resolve<IFilePickerService>();
            #pragma warning restore CS0618 // Type or member is obsolete

            var tabBarViewControllers = new[]
            {
                CreateTabViewController<TaskTabView, TaskViewModel>("Task", "icons8-news-30.png", 0, viewModel?.User, navigationService, taskService),
                CreateTabViewController<NewsTabView, NewsViewModel>("News", "icons8-envelope-30.png", 1, rssService),
                CreateTabViewController<UserProfileView, UserProfileViewModel>("Profile", "icons8-user-30.png", 2, viewModel?.User, navigationService, userService, filePickerService)
            };
            ViewControllers = tabBarViewControllers;
            SelectedViewController = tabBarViewControllers[0];
        }

        public override void ViewWillAppear(bool animated)
        {
            ViewModel.OnResume();
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion

        private UIViewController CreateTabViewController<TCtrl, TViewModel>(string title, string icon, nint index, params object[] services)
            where TCtrl : MvxViewController
            where TViewModel : MvxViewModel
        {
            if (Activator.CreateInstance(typeof(TCtrl)) is TCtrl viewCtrl)
            {
                viewCtrl.Title = title;
                viewCtrl.ViewModel = services != null && services.Length > 0 ?
               Activator.CreateInstance(typeof(TViewModel), services) as TViewModel :
               Activator.CreateInstance(typeof(TViewModel)) as TViewModel;

                viewCtrl.TabBarItem = new UITabBarItem(title, UIImage.FromFile(icon), index);
                return viewCtrl;
            }

            return null;
        }
    }
}