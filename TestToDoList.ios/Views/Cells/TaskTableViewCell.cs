﻿
using System;
using System.Drawing;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using TestToDoList.Core.Enums;
using TestToDoList.Core.Models;
using TestToDoList.Core.ViewModels;
using UIKit;

namespace TestToDoList.ios.Cells
{
    public partial class TaskTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("TaskTableViewCell");
        public static readonly UINib Nib;

        static TaskTableViewCell()
        {
            Nib = UINib.FromName("TaskTableViewCell", NSBundle.MainBundle);
        }

        protected TaskTableViewCell(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<TaskTableViewCell, TaskViewItem>();
                set.Bind(lblTitle).To(vm => vm.Heading);
                set.Bind(lblDeadline).To(vm => vm.Deadline);
                set.Bind(lblDescription).To(vm => vm.Description);
                set.Bind(lblStatus).To(vm => vm.Status);

                set.Apply();
            });
        } 
    }
}