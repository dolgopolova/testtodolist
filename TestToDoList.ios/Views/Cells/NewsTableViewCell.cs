﻿using System;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using TestToDoList.Core.Models;
using UIKit;

namespace TestToDoList.ios.Cells
{
    public partial class NewsTableViewCell : MvxTableViewCell
    {
        public static readonly NSString Key = new NSString("NewsTableViewCell");
        public static readonly UINib Nib;

        static NewsTableViewCell()
        {
            Nib = UINib.FromName("NewsTableViewCell", NSBundle.MainBundle);
        }

        protected NewsTableViewCell(IntPtr handle) : base(handle)
        {
            this.DelayBind(() =>
            {
                var set = this.CreateBindingSet<NewsTableViewCell, News>();

                set.Bind(lblTitle).To(vm => vm.Title);
                set.Bind(lblPubDate).To(vm => vm.PublishedDate);
                set.Bind(lblDescription).To(vm => vm.Description);
                
                set.Apply();
            });
        }
    }
}