// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TestToDoList.ios.Views
{
    [Register ("SignUp")]
    partial class SignUpView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnConfirm { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnEditImg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgProfile { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAlert { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tbxConfirmPassword { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tbxEmail { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tbxLastName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tbxLogin { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tbxPassword { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField tbxUserName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnConfirm != null) {
                btnConfirm.Dispose ();
                btnConfirm = null;
            }

            if (btnEditImg != null) {
                btnEditImg.Dispose ();
                btnEditImg = null;
            }

            if (imgProfile != null) {
                imgProfile.Dispose ();
                imgProfile = null;
            }

            if (lblAlert != null) {
                lblAlert.Dispose ();
                lblAlert = null;
            }

            if (tbxConfirmPassword != null) {
                tbxConfirmPassword.Dispose ();
                tbxConfirmPassword = null;
            }

            if (tbxEmail != null) {
                tbxEmail.Dispose ();
                tbxEmail = null;
            }

            if (tbxLastName != null) {
                tbxLastName.Dispose ();
                tbxLastName = null;
            }

            if (tbxLogin != null) {
                tbxLogin.Dispose ();
                tbxLogin = null;
            }

            if (tbxPassword != null) {
                tbxPassword.Dispose ();
                tbxPassword = null;
            }

            if (tbxUserName != null) {
                tbxUserName.Dispose ();
                tbxUserName = null;
            }
        }
    }
}