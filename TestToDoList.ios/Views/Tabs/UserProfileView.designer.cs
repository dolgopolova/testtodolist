// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TestToDoList.ios.Views.Tabs
{
    [Register ("UserProfileView")]
    partial class UserProfileView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnChangePassword { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton btnEditImg { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView imgProfile { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAlert { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txbEmail { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txbFirstName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txbLastName { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txbLogin { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (btnChangePassword != null) {
                btnChangePassword.Dispose ();
                btnChangePassword = null;
            }

            if (btnEditImg != null) {
                btnEditImg.Dispose ();
                btnEditImg = null;
            }

            if (imgProfile != null) {
                imgProfile.Dispose ();
                imgProfile = null;
            }

            if (lblAlert != null) {
                lblAlert.Dispose ();
                lblAlert = null;
            }

            if (txbEmail != null) {
                txbEmail.Dispose ();
                txbEmail = null;
            }

            if (txbFirstName != null) {
                txbFirstName.Dispose ();
                txbFirstName = null;
            }

            if (txbLastName != null) {
                txbLastName.Dispose ();
                txbLastName = null;
            }

            if (txbLogin != null) {
                txbLogin.Dispose ();
                txbLogin = null;
            }
        }
    }
}