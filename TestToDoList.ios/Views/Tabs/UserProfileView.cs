﻿
using System;
using System.Drawing;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using TestToDoList.Core.ViewModels;
using UIKit;

namespace TestToDoList.ios.Views.Tabs
{
    public partial class UserProfileView : MvxViewController<UserProfileViewModel>
    {
        UIButton _btnEdit;

        public UserProfileView() : base(nameof(UserProfileView), null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            Title = ViewModel.Title;

            _btnEdit = new UIButton();
            var btnSave = new UIButton();

            _btnEdit.SetImage(new UIImage("icons8-edit-account-30.png") , UIControlState.Normal);
            btnSave.SetImage(new UIImage("icons8-checkmark-30.png"), UIControlState.Normal);
            imgProfile.Image = new UIImage("noun_User.png");
            btnEditImg.SetImage(new UIImage("icons8-unsplash-30.png"), UIControlState.Normal);
            btnChangePassword.Hidden = true;
            
            var set = this.CreateBindingSet<UserProfileView, UserProfileViewModel>();

            if (ViewModel.ProfileImage != null)
            {
                var data = NSData.FromArray(ViewModel.ProfileImage);
                imgProfile.Image = UIImage.LoadFromData(data);
            }

            set.Bind(txbFirstName).To(vm => vm.FirstName);
            set.Bind(txbLastName).To(vm => vm.LastName);
            set.Bind(txbEmail).To(vm => vm.Email);
            set.Bind(txbLogin).To(vm => vm.Login);
            set.Bind(lblAlert).To(vm => vm.AlertMessage);
            set.Apply();

            txbEmail.EditingDidEndOnExit += (sender, args) => 
            {
                ViewModel.CheckEmail();
                ViewModel.AlertMessage = " ";
            };
            txbLogin.EditingDidEndOnExit += (sender, args) => 
            {
                ViewModel.CheckLogin();
                ViewModel.AlertMessage = " ";
            };
            
            var viewTap = new UITapGestureRecognizer(() =>
            {
                View.EndEditing(true);
            });
            View.AddGestureRecognizer(viewTap);

            _btnEdit.TouchDown += (sender, args) =>
            {
                TabBarController.NavigationItem.LeftBarButtonItem = new UIBarButtonItem(btnSave);
                btnChangePassword.Hidden = false;
                btnChangePassword.BackgroundColor = new UIColor(93f/255f, 187f/255f, 245f/255f, 255f/255f);
                SetMode(true);
            };

            btnSave.TouchDown += (sender, args) => 
            {
                if (string.IsNullOrWhiteSpace(lblAlert.Text))
                {
                    ViewModel.EditUserProfile();
                    TabBarController.NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_btnEdit);
                    btnChangePassword.Hidden = true;
                    btnChangePassword.BackgroundColor = UIColor.LightGray;
                    SetMode(false);
                }
            };

            btnChangePassword.TouchDown += (sender, args) => AddAlertController();
            
            btnEditImg.TouchDown += async (sender, args) =>
            {
                await ViewModel.UploadImageAsync();
                btnChangePassword.BackgroundColor = UIColor.LightGray;
                SetMode(false);
            };

            ViewModel.UploadImageFinally += UpdateProfileImage;
        }

        private void AddAlertController()
        {
            var alert = UIAlertController.Create("Change password", null, UIAlertControllerStyle.Alert);
            UITextField currentPassword = null, newPassword = null, confirmPassword = null;

            alert.AddTextField((textField) => {
                currentPassword = textField;
                currentPassword.Placeholder = "Current password";
                currentPassword.AutocorrectionType = UITextAutocorrectionType.No;
                currentPassword.KeyboardType = UIKeyboardType.Default;
                currentPassword.ReturnKeyType = UIReturnKeyType.Done;
                currentPassword.ClearButtonMode = UITextFieldViewMode.WhileEditing;
                currentPassword.BorderStyle = UITextBorderStyle.RoundedRect;
                currentPassword.SecureTextEntry = true;
            });
            alert.AddTextField((textField) => {
                newPassword = textField;
                newPassword.Placeholder = "New password";
                newPassword.AutocorrectionType = UITextAutocorrectionType.No;
                newPassword.KeyboardType = UIKeyboardType.Default;
                newPassword.ReturnKeyType = UIReturnKeyType.Done;
                newPassword.ClearButtonMode = UITextFieldViewMode.WhileEditing;
                newPassword.BorderStyle = UITextBorderStyle.RoundedRect;
                newPassword.SecureTextEntry = true;
            });
            alert.AddTextField((textField) => {
                confirmPassword = textField;
                confirmPassword.Placeholder = "Confirm password";
                confirmPassword.AutocorrectionType = UITextAutocorrectionType.No;
                confirmPassword.KeyboardType = UIKeyboardType.Default;
                confirmPassword.ReturnKeyType = UIReturnKeyType.Done;
                confirmPassword.ClearButtonMode = UITextFieldViewMode.WhileEditing;
                confirmPassword.BorderStyle = UITextBorderStyle.RoundedRect;
                confirmPassword.SecureTextEntry = true;
            });

            alert.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, async (actionOK) => {
                if (!string.IsNullOrWhiteSpace(currentPassword.Text) && !string.IsNullOrWhiteSpace(newPassword.Text)
                    && !string.IsNullOrWhiteSpace(confirmPassword.Text))
                {
                    ViewModel.CurrentPassword = currentPassword.Text;
                    ViewModel.NewPassword = newPassword.Text;
                    ViewModel.ConfirmPassword = confirmPassword.Text;
                    ViewModel.PasswordValidate();
                    await ViewModel.UploadImageAsync();
                }
                else lblAlert.Text = "Empty password field!";
            }));
            alert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Cancel, null));

            PresentViewController(alert, true, null);
        }

        private void UpdateProfileImage()
        {
            var data = NSData.FromArray(ViewModel.ProfileImage);
            imgProfile.Image = UIImage.LoadFromData(data);
        }

        public void SetMode(bool isEnabled)
        {
            txbFirstName.Enabled = isEnabled;
            txbLastName.Enabled = isEnabled;
            txbEmail.Enabled = isEnabled;
            txbLogin.Enabled = isEnabled;
            btnChangePassword.Enabled = isEnabled;
            btnEditImg.Enabled = isEnabled;
        }

        public override void ViewWillAppear(bool animated)
        {
            TabBarController.NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_btnEdit);
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}