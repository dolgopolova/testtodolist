﻿
using System;
using System.Drawing;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Views;
using TestToDoList.Core.ViewModels;
using TestToDoList.ios.Cells;
using TestToDoList.ios.Views.WorkWithTask;
using UIKit;

namespace TestToDoList.ios.Views.Tabs
{
    public partial class TaskTabView : MvxViewController<TaskViewModel>
    {
        UIButton _button;

        public TaskTabView() : base(nameof(TaskTabView), null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public async override void ViewDidLoad()
        {
            await ViewModel.Initialize();

            base.ViewDidLoad();

            _button = new UIButton();
            _button.SetImage(new UIImage("icons8-add-new-30.png"), UIControlState.Normal);

            var viewModel = ViewModel as TaskViewModel;

            var source = new TaskTabViewSource(tableTasks, viewModel);
            
            var set = this.CreateBindingSet<TaskTabView, TaskViewModel>();
            set.Bind(source).To(vm => vm.Tasks);
            set.Bind(_button).To(vm => vm.AddTaskCommand);
            set.Apply();

            tableTasks.Source = source;
           
            tableTasks.ContentInset = UIEdgeInsets.FromString("20.0, 20.0, 20.0, 20.0");
            tableTasks.RowHeight = 100;

            tableTasks.ReloadData();
        }

        public override void ViewWillAppear(bool animated)
        {
            TabBarController.NavigationItem.LeftBarButtonItem = new UIBarButtonItem(_button);
            ViewModel.OnResume();
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            tableTasks.BeginUpdates();
            tableTasks.ReloadRows(tableTasks.IndexPathsForVisibleRows, UITableViewRowAnimation.None);
            tableTasks.EndUpdates();
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}