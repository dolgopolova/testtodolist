﻿using System.Threading.Tasks;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Binding.Views;
using MvvmCross.Platforms.Ios.Views;
using TestToDoList.Core.ViewModels;
using TestToDoList.ios.Cells;
using UIKit;

namespace TestToDoList.ios.Views.Tabs
{
    public partial class NewsTabView : MvxViewController<NewsViewModel>
    {
        private bool _useRefreshControl = false;
        private UIRefreshControl _refreshControl;

        public UIRefreshControl RefreshControl => _refreshControl;
        public bool UseRefreshControll => _useRefreshControl;

        public NewsTabView() : base(nameof(NewsTabView), null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public async override void ViewDidLoad()
        {
            await ViewModel.Initialize();

            base.ViewDidLoad();

            var source = new MvxSimpleTableViewSource(tableNews, NewsTableViewCell.Key, NewsTableViewCell.Key);

            _refreshControl = new UIRefreshControl();

            var set = this.CreateBindingSet<NewsTabView, NewsViewModel>();
            set.Bind(source).To(vm => vm.News);
            set.Apply();

            await RefreshAsync();
            AddRefreshControl();

            tableNews.Add(RefreshControl);
            tableNews.Source = source;
            tableNews.ContentInset = UIEdgeInsets.FromString("20.0, 20.0, 20.0, 20.0");
            tableNews.RowHeight = 215;
            tableNews.ReloadData();
        }

        private void AddRefreshControl()
        {
            if (UIDevice.CurrentDevice.CheckSystemVersion(6, 0))
            {
                _refreshControl.ValueChanged += async (sender, e) =>
                {
                    await ViewModel.Initialize();
                    await RefreshAsync();
                };
                _useRefreshControl = true;
            }
        }

        private async Task RefreshAsync()
        {
            if (UseRefreshControll)
                RefreshControl.BeginRefreshing();

            if (UseRefreshControll)
                RefreshControl.EndRefreshing();
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            TabBarController.NavigationItem.LeftBarButtonItem = null;
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}