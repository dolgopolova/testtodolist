// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TestToDoList.ios.Views.Tabs
{
    [Register ("TaskTabView")]
    partial class TaskTabView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView tableTasks { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (tableTasks != null) {
                tableTasks.Dispose ();
                tableTasks = null;
            }
        }
    }
}