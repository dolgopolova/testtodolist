﻿using System;
using System.Drawing;
using CoreAnimation;
using CoreGraphics;
using Foundation;
using MPDCPastelXamarinIOS;
using UIKit;

namespace TestToDoList.ios.Views
{
    [Register("UILaunchScreenView")]
    public class UILaunchScreenView : UIView
    {
        //private CAGradientLayer _firstGradientLayer, _secondGradientLayer;

        public UILaunchScreenView()
        {
            Initialize();
        }

        public UILaunchScreenView(RectangleF bounds) : base(bounds)
        {
            Initialize();
        }

        void Initialize()
        {
            var pastelView = new PastelView(base.Bounds);
            pastelView.StartPoint = new CGPoint(base.Bounds.Top, base.Bounds.Top);
            pastelView.EndPoint = new CGPoint(1, 1);
            pastelView.AnimationDuration = 3.0;
            pastelView.SetColors(new UIColor[]
            {
                new UIColor(156f/255f, 39f/255f, 176f/255f, 1.0f),
                new UIColor(52f/255f, 191f/255f, 219f/255f, 1.0f),
                new UIColor(52f/255f, 91f/255f, 219f/255f, 1.0f),
                new UIColor(52f/255f, 219f/255f, 88f/255f, 1.0f)
            });
            pastelView.StartAnimation();
            base.InsertSubview(pastelView, 0);
        }
    }
}