﻿
using System;
using System.Drawing;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using TestToDoList.Core.ViewModels;
using UIKit;

namespace TestToDoList.ios.Views.WorkWithTask
{
    public partial class AddTaskView : MvxViewController<AddTaskViewModel>
    {
        UIBarButtonItem _button;

        public AddTaskView() : base(nameof(AddTaskView), null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            _button = new UIBarButtonItem();
            _button.Title = "Save";
            sgmStatus.SelectedSegment = ViewModel.Status;

            var set = this.CreateBindingSet<AddTaskView, AddTaskViewModel>();
            set.Bind(txbTitle).To(vm => vm.Heading);
            set.Bind(txbDeadline).To(vm => vm.Deadline);
            set.Bind(txvDescription).To(vm => vm.Description);
            set.Bind(lblAlert).To(vm => vm.AlertMessage);
            set.Apply();

            var datePicker = new UIDatePicker()
            {
                MinimumDate = (NSDate)DateTime.Now
            };
            datePicker.BackgroundColor = UIColor.White;
            datePicker.Mode = UIDatePickerMode.DateAndTime;
            txbDeadline.InputView = datePicker;
            datePicker.ValueChanged += (object sender, EventArgs e) =>
            {
                txbDeadline.Text = Convert.ToDateTime(datePicker.Date.ToString()).ToString("MM/dd/yyyy hh:mm");
                ViewModel.Deadline = txbDeadline.Text;
            };

            _button.Clicked += (sender, e) =>
            {
                ViewModel.CheckFieldsCommand.Execute(null);
                if (string.IsNullOrWhiteSpace(lblAlert.Text))
                {
                    ViewModel.SaveTaskCommand.Execute(null);
                }
            };

            var viewTap = new UITapGestureRecognizer(() =>
            {
                View.EndEditing(true);
            });
            View.AddGestureRecognizer(viewTap);
        }

        partial void SegmentControl_ValueChanged(UIKit.UISegmentedControl sender)
        {
            var indexSelectedSegment = sgmStatus.SelectedSegment;
            ViewModel.Status = (int)indexSelectedSegment;
        }

        public override void ViewWillAppear(bool animated)
        {
            NavigationItem.RightBarButtonItem = _button;
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}