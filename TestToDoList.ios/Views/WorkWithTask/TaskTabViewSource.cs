﻿
using System;
using System.Drawing;

using Foundation;
using MvvmCross.Platforms.Ios;
using MvvmCross.Platforms.Ios.Binding.Views;
using TestToDoList.Core.Enums;
using TestToDoList.Core.Models;
using TestToDoList.Core.ViewModels;
using TestToDoList.ios.Cells;
using UIKit;

namespace TestToDoList.ios.Views.WorkWithTask
{
    public partial class TaskTabViewSource : MvxTableViewSource
    {

        public TaskViewModel ViewModel { get; }

        public  TaskTabViewSource(UITableView tableView, TaskViewModel viewModel, NSBundle bundle = null)
            : base(tableView)
        {
            ViewModel = viewModel;

            tableView.RegisterNibForCellReuse(UINib.FromName(TaskTableViewCell.Key, bundle ?? NSBundle.MainBundle),
                TaskTableViewCell.Key);
        }

        protected override UITableViewCell GetOrCreateCellFor(UITableView tableView, NSIndexPath indexPath, object item)
        {
            var taskModel = item as TaskModel;

            var cell = tableView.DequeueReusableCell(TaskTableViewCell.Key) as TaskTableViewCell;

            return cell;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return 100 + 1;
        }

        #region editing methods

        public override void CommitEditingStyle(UITableView tableView, UITableViewCellEditingStyle editingStyle, NSIndexPath indexPath)
        {
            // get task and remind id
            var task = ViewModel.Tasks[indexPath.Row];

            switch (editingStyle)
            {
                case UITableViewCellEditingStyle.Delete:
                    // remove the item from the underlying data source
                    ViewModel.Tasks.RemoveAt(indexPath.Row);
                    //delete task from the database
                    ViewModel.DeleteTask(task.Id);
                    break;
                case UITableViewCellEditingStyle.None:
                    Console.WriteLine("CommitEditingStyle:None called");
                    break;
            }
        }

        public async void Initialise()
        {
            await ViewModel.Initialize();
        }

        public override UISwipeActionsConfiguration GetLeadingSwipeActionsConfiguration(UITableView tableView, NSIndexPath indexPath)
        {
            var editAction = ContextualEditAction(indexPath.Row);
            var leadingSwipe = UISwipeActionsConfiguration.FromActions(new UIContextualAction[] { editAction });

            leadingSwipe.PerformsFirstActionWithFullSwipe = false;
            return leadingSwipe;
        }

        private UIContextualAction ContextualEditAction(int row)
        {
            TaskStatusType status = TaskStatusType.InProgress;
            switch(ViewModel.Tasks[row].Status)
            {
                case "Not started":
                    status = TaskStatusType.NotStarted;
                    break;
                case "In progress":
                    status = TaskStatusType.InProgress;
                    break;
                case "Done":
                    status = TaskStatusType.Done;
                    break;
            }

            var task = new TaskModel
            {
                Id = ViewModel.Tasks[row].Id,
                UserId = ViewModel.Tasks[row].UserId,
                Heading = ViewModel.Tasks[row].Heading,
                Deadline = ViewModel.Tasks[row].Deadline,
                Description = ViewModel.Tasks[row].Description,
                Status = status
            };
            var action = UIContextualAction.FromContextualActionStyle(UIContextualActionStyle.Normal,
                                                                      "Edit",
                                                                      (EditAction, view, success) =>
                                                                      {
                                                                          ViewModel.EditTaskAsync(task);
                                                                          success(true);
                                                                      });

            action.Image = UIImage.FromFile("icons8-task-30.png");

            action.BackgroundColor = UIColor.DarkGray;

            return action;
        }

        public override string TitleForDeleteConfirmation(UITableView tableView, NSIndexPath indexPath)
        {
            return "Delete";
        }

        public override bool CanEditRow(UITableView tableView, NSIndexPath indexPath)
        {
            return true;
        }

        public override bool CanMoveRow(UITableView tableView, NSIndexPath indexPath)
        {
            return false;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            TaskStatusType status = TaskStatusType.InProgress;
            switch (ViewModel.Tasks[indexPath.Row].Status)
            {
                case "Not started":
                    status = TaskStatusType.NotStarted;
                    break;
                case "In progress":
                    status = TaskStatusType.InProgress;
                    break;
                case "Done":
                    status = TaskStatusType.Done;
                    break;
            }

            var taskViewItem = new TaskModel
            {
                Id = ViewModel.Tasks[indexPath.Row].Id,
                UserId = ViewModel.Tasks[indexPath.Row].UserId,
                Heading = ViewModel.Tasks[indexPath.Row].Heading,
                Deadline = ViewModel.Tasks[indexPath.Row].Deadline,
                Description = ViewModel.Tasks[indexPath.Row].Description,
                Status = status
            };
            ViewModel.EditTaskAsync(taskViewItem);
        }
        #endregion
    }
}