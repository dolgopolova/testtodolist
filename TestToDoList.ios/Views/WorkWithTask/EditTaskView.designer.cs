// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;

namespace TestToDoList.ios.Views
{
    [Register ("EditTaskView")]
    partial class EditTaskView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel lblAlert { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UISegmentedControl sgmStatus { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txbDeadline { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextField txbTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView txvDescription { get; set; }

        [Action ("SegmenteContol_ValueChanged:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void SegmenteContol_ValueChanged (UIKit.UISegmentedControl sender);

        void ReleaseDesignerOutlets ()
        {
            if (lblAlert != null) {
                lblAlert.Dispose ();
                lblAlert = null;
            }

            if (sgmStatus != null) {
                sgmStatus.Dispose ();
                sgmStatus = null;
            }

            if (txbDeadline != null) {
                txbDeadline.Dispose ();
                txbDeadline = null;
            }

            if (txbTitle != null) {
                txbTitle.Dispose ();
                txbTitle = null;
            }

            if (txvDescription != null) {
                txvDescription.Dispose ();
                txvDescription = null;
            }
        }
    }
}