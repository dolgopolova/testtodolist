﻿
using System;
using System.Drawing;

using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using TestToDoList.Core.ViewModels;
using UIKit;

namespace TestToDoList.ios.Views
{
    public partial class SignUpView : MvxViewController<SignUpViewModel>
    {
        public SignUpView() : base(nameof(SignUpView), null)
        {
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        #region View lifecycle

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            imgProfile.Image = new UIImage("noun_User");
            btnEditImg.SetImage(new UIImage("icons8-unsplash-30.png"), UIControlState.Normal);
            Title = ViewModel.Title;

            tbxEmail.ResignFirstResponder();
            tbxLogin.ResignFirstResponder();

            var set = this.CreateBindingSet<SignUpView, SignUpViewModel>();
            set.Bind(tbxEmail).To(vm => vm.Email);
            set.Bind(tbxLogin).To(vm => vm.Login);
            set.Bind(tbxPassword).To(vm => vm.Password);
            set.Bind(tbxConfirmPassword).To(vm => vm.ConfirmPassword);
            set.Bind(tbxUserName).To(vm => vm.UserName);
            set.Bind(tbxLastName).To(vm => vm.LastName);
            set.Bind(lblAlert).To(vm => vm.AlertMessage);
            set.Bind(btnConfirm).To(vm => vm.ConfirmSignUpCommand);
            set.Bind(btnEditImg).To(vm => vm.UploadImageCommand);
            set.Apply();

            var viewTap = new UITapGestureRecognizer(() =>
            {
                View.EndEditing(true);
            });
            View.AddGestureRecognizer(viewTap);

            tbxEmail.EditingDidEnd += (sender, args) => { ViewModel.CheckEmail(); };
            tbxLogin.EditingDidEnd += (sender, args) => { ViewModel.CheckLogin(); };
            btnConfirm.TouchDown += (sender, args) => { ViewModel.Validate(); };

            ViewModel.UploadImageFinally += UpdateProfileImage;
        }

        private void UpdateProfileImage()
        {
            var data = NSData.FromArray(ViewModel.ProfileImage);
            imgProfile.Image = UIImage.LoadFromData(data);
        }

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
        }

        public override void ViewWillDisappear(bool animated)
        {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);
        }

        #endregion
    }
}