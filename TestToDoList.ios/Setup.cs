﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using MvvmCross;
using MvvmCross.Platforms.Ios.Core;
using MvvmCross.ViewModels;
using TestToDoList.Core;
using TestToDoList.Core.Services;
using TestToDoList.iOS.Services;
using UIKit;

namespace TestToDoList.ios
{
    public class Setup : MvxIosSetup<App>
    {
        protected override IMvxApplication CreateApp()
        {
#pragma warning disable CS0618 // Type or member is obsolete
            Mvx.LazyConstructAndRegisterSingleton<ISQLite, SQLiteIos>();
            Mvx.LazyConstructAndRegisterSingleton<IFilePickerService, FilePickerService>();
#pragma warning restore CS0618 // Type or member is obsolete
            return new App();
        }
    }
}